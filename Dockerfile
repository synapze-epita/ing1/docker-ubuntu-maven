FROM ubuntu:18.04

RUN apt-get update \
    && apt-get install -y maven openjdk-8-jdk

RUN update-java-alternatives --set java-1.8.0-openjdk-amd64
